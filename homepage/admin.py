from django.contrib import admin

# Register your models here.
from .models import ClassYear, Friend
admin.site.register(ClassYear)
admin.site.register(Friend)