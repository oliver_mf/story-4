from django import forms
from .models import Friend, ClassYear

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'ClassYear', 'hobby', 'favFoodDrink']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
