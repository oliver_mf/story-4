from django.urls import path
from . import views
from django.contrib import admin

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('addFriend/', views.addFriend, name='addFriend'),
    path('seeFriend/', views.seeFriend, name='seeFriend'),
]