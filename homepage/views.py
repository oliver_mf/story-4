from django.shortcuts import render
from django.shortcuts import redirect
from .models import Friend, ClassYear
from . import forms

# Create your views here.
def index(request):
    return render(request, 'index.html')

def seeFriend(request):
    friend = Friend.objects.all()
    classyear = ClassYear.objects.all()
    response = {'friend' : friend, 'classyear': classyear}
    return render(request, 'seeFriend.html', response)

def addFriend(request):
    if request.method == 'POST':
        form = forms.FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:seeFriend')
    else:
        form = forms.FriendForm()
    return render(request, 'addFriend.html', {'form': form})

